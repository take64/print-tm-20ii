const escpos = require('escpos');

const device = new escpos.USB();
const printer = new escpos.Printer(device);

device.open(function() {
  printer
    .text('Bienvenidos a Intercable')
    .cut()
    .close();
});
